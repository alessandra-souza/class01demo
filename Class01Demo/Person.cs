﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class01Demo
{
    class Person
    {
        //public Person (string firstName)
        //shorthand for getter and setter
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName
        {

            get
            {
                //interpolating the variables
                //return $"{ FirstName } { LastName }";

                //using placeholders for variables
                return String.Format("{0} {1}", FirstName, LastName);
            }
        }
            

    }
}
