﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Class01Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// Alessandra Ferreira de Souza

    public partial class MainWindow : Window
    {

        List<Person> people = new List<Person>();

        public MainWindow()
        {
            InitializeComponent();

            people.Add(new Person { FirstName = "Natasha", LastName = "Romanoff" });
            people.Add(new Person { FirstName = "Bruce", LastName = "Romanoff" });
            people.Add(new Person { FirstName = "Tony", LastName = "Romanoff" });

            cbxNames.ItemsSource = people;
        }

        private void btnRun_Click(object sender, RoutedEventArgs e)
        {
            string firstName = tbFirstName.Text;

            MessageBox.Show(firstName);
        }

        private void btnChange_Name_Click(object sender, RoutedEventArgs e)
        {

            title.Text = "Alessandra";

        }

        //method to clear the checked list items when the button is clicked
        private void btnClearList_Click(object sender, RoutedEventArgs e)
        {
            
            foreach (object o in sp.Children)
            {
                if (o is CheckBox)
                {
                    ((CheckBox)o).IsChecked = false;
                }
            }

        }
    }
}
